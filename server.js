const express = require('express')
const axios = require('axios')
const app = express()
const bodyParser = require('body-parser')
const mysql = require('mysql')
const cors = require('cors')
const res = require('express/lib/response')
const net = require("net")

//app.use(bodyParser.urlencoded({ extended: false }))
//app.use(bodyParser.json())
app.use(cors())
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}))
app.use(bodyParser.json({limit: '50mb', extended: true}))
app.use(express.json())

const db = mysql.createConnection({
    user: "root",
    host: "localhost",
    password: "",
    database: "mock_service"
})

app.get('/all',(req,res) => {
    db.query("SELECT * FROM newdata", (err, result) => {
        if(err){
            console.log(err)
        }else{
            res.send(result)
        }
    })
})

app.get('/report/all',(req,res) => {
    db.query("SELECT * FROM report", (err, result) => {
        if(err){
            console.log(err)
        }else{
            res.send(result)
        }
    })
})

app.post('/add', (req,res) => {
    const text = req.body.text

    db.query("INSERT INTO newdata (text) VALUES (?)",
    [text],
    (err, result) => {
        if(text != null){
            if(err){
                console.log(err)
            }else{
                res.send(result)
                console.log(result)
            }
        }else{
            res.send("Text null")
        }
    })
})

app.post('/report/add', (req,res) => {
    const title = req.body.title
    const environment = req.body.environment
    const functionvalue = req.body.functionvalue
    const platform = req.body.platform
    const detail = req.body.detail
    const comment = req.body.comment
    const comment2 = req.body.comment2
    const comment3 = req.body.comment3
    const comment4 = req.body.comment4
    const imageList = req.body.image

    db.query("INSERT INTO report (title, environment, functionvalue, platform, detail, comment, comment2, comment3, comment4) VALUES (?,?,?,?,?,?,?,?,?)",
    [title, environment, functionvalue, platform, detail, comment, comment2, comment3, comment4],
    (err, result) => {
        if(title != null){
            if(err){
                console.log(err)
            }else{
                const values = []
                for(var i = 0 ; i < imageList.length; i++){
                    values.push([result.insertId , imageList[i]])
                }

                console.log("Data id values : "+values)
                db.query("INSERT INTO images (rp_id, image) VALUE ?", [values],
                    (err,resq) => {
                        if(err){
                            console.log(err)
                        }else{
                            res.send(result)
                            console.log(result)
                            sendNotify(req,result)
                        }
                    })
            }
        }else{
            res.send("Text null")
        }
    })
})

app.post('/get', (req,res) => {
    const getId = req.body.id

    db.query("SELECT * FROM newdata WHERE id = "+getId, (err, result) => {
        if(err){
            console.log(err)
        }else{
            res.send(result[0])
            console.log(result)
        }
    })
})

function sendNotify(req,res){
    const axios = require('axios');
    const querystring = require('querystring');
    const title = req.body.title
    const environment = req.body.environment
    const functionvalue = req.body.functionvalue
    const platform = req.body.platform
    const detail = req.body.detail
    const comment = req.body.comment
    const comment2 = req.body.comment2
    const comment3 = req.body.comment3
    const comment4 = req.body.comment4
    const image = req.body.image
    const insertId = res.insertId

    axios({
        method: 'post',
        url: 'https://notify-api.line.me/api/notify',
        headers: {
        'Authorization': 'Bearer ' + 'ZaGnXLhBSUY3dAjouL8nZqdqvulAWTUZZT9L8Z5kkb7',
        'Content-Type': 'application/x-www-form-urlencoded'
        },
        data: querystring.stringify({
        message: "Add Report"
                +"\nid : "+insertId
                +"\ntitle : "+title
                +"\nenvironment : "+environment
                +"\nfunctionvalue : "+functionvalue
                +"\nplatform : "+platform
                +"\ndetail : "+detail
                +"\ncomment : "+comment
                +"\ncomment2 : "+comment2
                +"\ncomment3 : "+comment3
                +"\ncomment4 : "+comment4
                +"\nimage : "+image,
        })
    })
    .then( function(res) {
    console.log(res.data);
    })
    .catch( function(err) {
    console.error(err);
    });
}

app.listen(4321, () => {
    console.log("Start server at port 4321.")
})